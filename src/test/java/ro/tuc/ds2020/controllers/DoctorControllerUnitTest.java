package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.CareGiverDetailsDTO;
import ro.tuc.ds2020.dtos.DoctorDtoDetails;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.services.CareGiverService;
import ro.tuc.ds2020.services.DoctorService;

import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DoctorControllerUnitTest extends Ds2020TestConfig {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DoctorService doctorService;

    @Test
    public void insertDoctorTest() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        Date date = new Date(1878,12,20);
        DoctorDtoDetails doctorDtoDetails = new DoctorDtoDetails("Mihai Eminovici", "Luceafarului", date,"male");

        mockMvc.perform(post("/doctor")
                .content(objectMapper.writeValueAsString(doctorDtoDetails))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

    @Test
    public void insertDoctorTestFailsDueToNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        Date date = new Date(1998,10,29);
        DoctorDtoDetails careGiverDetailsDTO = new DoctorDtoDetails("John", "Somewhere Else street", date,"male");

        mockMvc.perform(post("/doctor")
                .content(objectMapper.writeValueAsString(careGiverDetailsDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }
}
