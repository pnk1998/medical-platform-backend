package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.CareGiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.services.CareGiverService;
import ro.tuc.ds2020.services.PatientService;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CareGiverControllerUnitTest extends Ds2020TestConfig {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CareGiverService service;

    @Test
    public void insertCareGiverTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        Date date = new Date(1998,10,29);
        CareGiverDetailsDTO careGiverDetailsDTO = new CareGiverDetailsDTO("John", "Somewhere Else street", date,"male");

        mockMvc.perform(post("/caregiver")
                .content(objectMapper.writeValueAsString(careGiverDetailsDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

    @Test
    public void insertCareGiverTestFailsDueToNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

            Date date = new Date(1998,10,29);
            CareGiverDetailsDTO careGiverDetailsDTO = new CareGiverDetailsDTO("John", "Somewhere Else street", date,"male");

            mockMvc.perform(post("/caregiver")
                    .content(objectMapper.writeValueAsString(careGiverDetailsDTO))
                    .contentType("application/json"))
                    .andExpect(status().isCreated());
    }

}
