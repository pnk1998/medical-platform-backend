package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.PatientService;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PatientControllerUnitTest extends Ds2020TestConfig {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PatientService service;

    @Test
    public void insertPatientTest() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        Date date = new Date(1998,10,29);
        PatientDetailsDTO patientDTO = new PatientDetailsDTO("John", "Somewhere Else street", date,"male","crazy");

        mockMvc.perform(post("/patient")
                .content(objectMapper.writeValueAsString(patientDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }

}
