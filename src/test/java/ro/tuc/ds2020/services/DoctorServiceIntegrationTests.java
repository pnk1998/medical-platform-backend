package ro.tuc.ds2020.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.CareGiverDTO;
import ro.tuc.ds2020.dtos.CareGiverDetailsDTO;
import ro.tuc.ds2020.dtos.DoctorDto;
import ro.tuc.ds2020.dtos.DoctorDtoDetails;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.util.AssertionErrors.assertEquals;

public class DoctorServiceIntegrationTests extends Ds2020TestConfig {
    @Autowired

    DoctorService doctorService;

    @Test
    public void testGetCorrect() {
        List<DoctorDto> careGiverDTOList = doctorService.findDoctors();
        assertEquals("Test Insert Doctors", 2, careGiverDTOList.size());
    }

    @Test
    public void testInsertCorrectWithGetById() {
        Date date = new Date(1998,10,29);
        DoctorDtoDetails p = new DoctorDtoDetails("John", "Somewhere Else street", date,"male");
        UUID insertedID = doctorService.insert(p);

        DoctorDtoDetails insertedCareGiver = new DoctorDtoDetails(insertedID, p.getName(),p.getAddress(), p.getDate_of_birth(),p.getGender());
        DoctorDtoDetails fetchedCareGiver = doctorService.findDoctorById(insertedID);

        assertEquals("Test Inserted Doctor", insertedCareGiver,fetchedCareGiver);
    }

    @Test
    public void testInsertCorrectWithGetAll() {
        Date date = new Date(1998,10,29);
        DoctorDtoDetails p = new DoctorDtoDetails("John", "Somewhere Else street", date,"male");
        doctorService.insert(p);

        List<DoctorDto> careGiverDTOList = doctorService.findDoctors();
        assertEquals("Test Inserted Doctors", 1, careGiverDTOList.size());
    }
}
