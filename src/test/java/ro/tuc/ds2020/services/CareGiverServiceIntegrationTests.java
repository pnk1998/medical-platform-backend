package ro.tuc.ds2020.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.CareGiverDTO;
import ro.tuc.ds2020.dtos.CareGiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.entities.CareGiver;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.util.AssertionErrors.assertEquals;

public class CareGiverServiceIntegrationTests extends Ds2020TestConfig {

    @Autowired

    CareGiverService careGiverService;

    @Test
    public void testGetCorrect() {
        List<CareGiverDTO> careGiverDTOList = careGiverService.findCareGiver();
        assertEquals("Test Insert CareGiver", 2, careGiverDTOList.size());
    }

    @Test
    public void testInsertCorrectWithGetById() {
        Date date = new Date(1998,10,29);
        CareGiverDetailsDTO p = new CareGiverDetailsDTO("John", "Somewhere Else street", date,"male");
        UUID insertedID = careGiverService.insert(p);

        CareGiverDetailsDTO insertedCareGiver = new CareGiverDetailsDTO(insertedID, p.getName(),p.getAddress(), p.getDate_of_birth(),p.getGender());
        CareGiverDetailsDTO fetchedCareGiver = careGiverService.findCareGiverById(insertedID);

        assertEquals("Test Inserted CareGiver", insertedCareGiver,fetchedCareGiver);
    }

    @Test
    public void testInsertCorrectWithGetAll() {
        Date date = new Date(1998,10,29);
        CareGiverDetailsDTO p = new CareGiverDetailsDTO("John", "Somewhere Else street", date,"male");
        careGiverService.insert(p);

        List<CareGiverDTO> careGiverDTOList = careGiverService.findCareGiver();
        assertEquals("Test Inserted CareGivers", 1, careGiverDTOList.size());
    }
}
