package ro.tuc.ds2020.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.util.AssertionErrors.assertEquals;

public class PatientServiceIntegrationTests extends Ds2020TestConfig {
    @Autowired

    PatientService patientService;

    @Test
    public void testGetCorrect() {
        List<PatientDTO> patientDTOList = patientService.findPatients();
        assertEquals("Test Insert Patient", 2, patientDTOList.size());
    }

    @Test
    public void testInsertCorrectWithGetById() {
        Date date = new Date(1998,10,29);
        PatientDetailsDTO p = new PatientDetailsDTO("John", "Somewhere Else street", date,"male","crazy");
        UUID insertedID = patientService.insert(p);

        PatientDetailsDTO insertedPatient = new PatientDetailsDTO(insertedID, p.getName(),p.getAddress(), p.getDate(),p.getGender(),p.getMed_rec());
        PatientDetailsDTO fetchedPatient = patientService.findPatientById(insertedID);

        assertEquals("Test Inserted Patient", insertedPatient, fetchedPatient);
    }

    @Test
    public void testInsertCorrectWithGetAll() {
        Date date = new Date(1998,10,29);
        PatientDetailsDTO p = new PatientDetailsDTO("John", "Somewhere Else street", date,"male","crazy");
        patientService.insert(p);

        List<PatientDTO> patientDTOList = patientService.findPatients();
        assertEquals("Test Inserted Patients", 1, patientDTOList.size());
    }
}
