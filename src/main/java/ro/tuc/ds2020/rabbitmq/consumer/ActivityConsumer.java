package ro.tuc.ds2020.rabbitmq.consumer;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.ActivityStatus;
import ro.tuc.ds2020.rabbitmq.conf.MessagingConfiguration;

@Component
public class ActivityConsumer {

    @RabbitListener(queues = MessagingConfiguration.QUEUE)
    public void consumerMessageFrom(ActivityStatus activityStatus){

        if(activityStatus.getActivity().getName().contentEquals("Sleeping")){
            long start = activityStatus.getActivity().getStart().getTime();
            long end = activityStatus.getActivity().getEnd().getTime();

            long result = end - start;

            if(result > 25200000){
                System.out.println("He slept more than 7 hours !!!");
            }
        }

        else if(activityStatus.getActivity().getName().contains("Leaving")){
            System.out.println("HELLOOOO");
            long start = activityStatus.getActivity().getStart().getTime();
            long end = activityStatus.getActivity().getEnd().getTime();

            long result = end - start;

            if(result > 18000000){
                System.out.println("He was outdoor more than 5 hours !!!");
            }
        }

        else if(activityStatus.getActivity().getName().contains("Toileting")) {
            long start = activityStatus.getActivity().getStart().getTime();
            long end = activityStatus.getActivity().getEnd().getTime();

            long result = end - start;

            if (result > 1800000) {
                System.out.println("He was at bathroom more than 30 minutes !!!");
            }
        }

        System.out.println("Message recieved from rabbitmq: "+activityStatus);

    }
}