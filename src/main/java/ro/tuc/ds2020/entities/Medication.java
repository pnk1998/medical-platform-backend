package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
public class Medication implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "effects")
    private String effects;

    @Column(name = "dosage", nullable = false)
    private int dosage;

    @ManyToOne
    @JoinColumn(name= "id_medPlan")
    private MedPlan medPlan;

    public Medication(){

    }
    public Medication(String name, String effects, int dosage) {
        this.name = name;
        this.effects = effects;
        this.dosage = dosage;
    }

    public Medication(UUID id, String name, String effects, int dosage) {
        this.id = id;
        this.name = name;
        this.effects = effects;
        this.dosage = dosage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Medication that = (Medication) o;
        return dosage == that.dosage &&
                id.equals(that.id) &&
                name.equals(that.name) &&
                effects.equals(that.effects);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, effects, dosage);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffects() {
        return effects;
    }

    public void setEffects(String effects) {
        this.effects = effects;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public MedPlan getMedPlan() {
        return medPlan;
    }

    public void setMedPlan(MedPlan medPlan) {
        this.medPlan = medPlan;
    }

    @Override
    public String toString() {
        return "Medication{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", effects='" + effects + '\'' +
                ", dosage=" + dosage +
                '}';
    }
}
