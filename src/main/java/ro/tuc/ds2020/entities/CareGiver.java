package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
public class CareGiver implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "date_of_birth", nullable = false)
    private Date date_of_birth;

    @Column(name = "gender", nullable = false)
    private String gender;

//    @OneToMany (mappedBy = "careGiver", targetEntity = Patient.class)
//    @Column(name = "patients")
//    private List<Patient> patientList = new ArrayList<>();


    public CareGiver(String name, String address, Date date_of_birth, String gender) {
        this.name = name;
        this.address = address;
        this.date_of_birth = date_of_birth;
        this.gender = gender;
    }

 /*   public CareGiver(String name, String address, Date date_of_birth, String gender) {
        this.name = name;
        this.address = address;
        this.date_of_birth = date_of_birth;
        this.gender = gender;
        this.patientList = patientList;
    }
*/
    public CareGiver(UUID id,String name, String address, Date date_of_birth, String gender) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.date_of_birth = date_of_birth;
        this.gender = gender;

    }

 /*   public CareGiver(UUID id,String name, String address, Date date_of_birth, String gender, List<Patient> patientList) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.date_of_birth = date_of_birth;
        this.gender = gender;
        this.patientList = patientList;
    }
*/
    public CareGiver(){

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date_of_birth;
    }

    public void setDate(Date date) {
        this.date_of_birth = date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

//    public List<Patient> getPatientList() {
//        return patientList;
//    }
//
//    public void setPatientList(List<Patient> patientList) {
//        this.patientList = patientList;
//    }

    @Override
    public String toString() {
        return "CareGiver{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", date_of_birth=" + date_of_birth +
                ", gender='" + gender + '\'' +
                '}';
    }

    public String toString1() {
        return "CareGiver{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", date_of_birth=" + date_of_birth +
                ", gender='" + gender + '\'' +
                '}';
    }

}
