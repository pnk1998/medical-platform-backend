package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
public class Patient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "date_of_birth", nullable = false)
    private Date date;

    @Column(name = " gender", nullable = false)
    private String gender;

    @Column(name = "medical_record", nullable = false)
    private String med_rec;

    @ManyToOne
    @JoinColumn(name = "careGiver")
    private CareGiver careGiver;

    @ManyToOne
    @JoinColumn(name = "id_medPlan")
    private MedPlan medPlan;

    public Patient() {
    }

    public Patient(String name, String address, Date date, String gender, String med_rec) {
        this.name = name;
        this.address = address;
        this.date = date;
        this.gender = gender;
        this.med_rec = med_rec;
    }

    public Patient(UUID id, String name, String address, Date date, String gender, String med_rec) {
        this.name = name;
        this.address = address;
        this.date = date;
        this.gender = gender;
        this.med_rec = med_rec;
        this.id = id;
    }

    public Patient(String name, String address, Date date, String gender, String med_rec, CareGiver careGiver) {
        this.name = name;
        this.address = address;
        this.date = date;
        this.gender = gender;
        this.med_rec = med_rec;
        this.careGiver = careGiver;
    }

    public MedPlan getMedPlan() {
        return medPlan;
    }

    public void setMedPlan(MedPlan medPlan) {
        this.medPlan = medPlan;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMed_rec() {
        return med_rec;
    }

    public void setMed_rec(String med_rec) {
        this.med_rec = med_rec;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public CareGiver getCareGiver() {
        return careGiver;
    }

    public void setCareGiver(CareGiver careGiver) {
        this.careGiver = careGiver;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", date=" + date +
                ", gender='" + gender + '\'' +
                ", med_rec='" + med_rec + '\'' +
                ", careGiver=" + careGiver +
                ", medPlan=" + medPlan +
                '}';
    }

    public String toString1() {
        return "Patient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", date=" + date +
                ", gender='" + gender + '\'' +
                ", med_rec='" + med_rec + '\'' +
                '}';
    }
}
