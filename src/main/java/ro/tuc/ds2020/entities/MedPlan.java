package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
public class MedPlan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id_medPlan;

    @Column(name = "begin_hour_for_pills", nullable = false)
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection(targetClass = Time.class)
    private List<Time> start_hour;


    @Column(name = "end_hour_for_pills", nullable = false)
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection(targetClass = Time.class)
    private List<Time> end_hour;

    @Column(name = "begin_period_of_treatment", nullable = false)
    private Date start_period;

    @Column(name = "end_period_of_treatment",nullable = false)
    private Date end_period;

    @OneToMany (mappedBy = "medPlan", targetEntity = Medication.class, fetch = FetchType.EAGER)
    @Column(name = "medication_list", nullable =  false)
    private List<Medication> medicationList = new ArrayList<>();

    public MedPlan(List <Time> start_hour,List <Time> end_hour,Date end_period,Date start_period, List<Medication> medicationList) {
        this.start_hour = start_hour;
        this.end_hour = end_hour;
        this.start_period = start_period;
        this.end_period = end_period;
        this.medicationList = medicationList;
    }

    public MedPlan(UUID id,List <Time> start_hour,List <Time> end_hour,Date end_period,Date start_period, List<Medication> medicationList) {
        this.id_medPlan = id;
        this.start_hour = start_hour;
        this.end_hour = end_hour;
        this.start_period = start_period;
        this.end_period = end_period;
        this.medicationList = medicationList;
    }

    public MedPlan(){

    }

    public MedPlan(List <Time> start_hour,List <Time> end_hour,Date end_period,Date start_period) {
        this.start_hour = start_hour;
        this.end_hour = end_hour;
        this.start_period = start_period;
        this.end_period = end_period;
    }

    public MedPlan(UUID id ,List <Time> start_hour,List <Time> end_hour,Date end_period,Date start_period) {
        this.id_medPlan = id;
        this.start_hour = start_hour;
        this.end_hour = end_hour;
        this.start_period = start_period;
        this.end_period = end_period;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public UUID getId() {
        return id_medPlan;
    }

    public void setId(UUID id) {
        this.id_medPlan = id;
    }

    public List<Time> getStart_hour() {
        return start_hour;
    }

    public void setStart_hour(List<Time> start_hour) {
        this.start_hour = start_hour;
    }

    public List<Time> getEnd_hour() {
        return end_hour;
    }

    public void setEnd_hour(List<Time> end_hour) {
        this.end_hour = end_hour;
    }

    public Date getStart_period() {
        return start_period;
    }

    public void setStart_period(Date start_period) {
        this.start_period = start_period;
    }

    public Date getEnd_period() {
        return end_period;
    }

    public void setEnd_period(Date end_period) {
        this.end_period = end_period;
    }

    @Override
    public String toString() {
        return "MedPlan{" +
                "id_medPlan=" + id_medPlan +
                ", start_hour=" + start_hour +
                ", end_hour=" + end_hour +
                ", start_period=" + start_period +
                ", end_period=" + end_period +
                ", medicationList=" + medicationList +
                '}';
    }
}
