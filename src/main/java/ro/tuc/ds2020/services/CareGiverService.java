package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CareGiverDTO;
import ro.tuc.ds2020.dtos.CareGiverDetailsDTO;
import ro.tuc.ds2020.dtos.builders.CareGiverBuilder;
import ro.tuc.ds2020.entities.CareGiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.CareGiverRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CareGiverService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CareGiverService.class);
    private final CareGiverRepository careGiverRepository;

    @Autowired
    public CareGiverService(CareGiverRepository careGiverRepository) {
        this.careGiverRepository = careGiverRepository;
    }

    public List<CareGiverDTO> findCareGiver(){
        List<CareGiver> careGiverList = careGiverRepository.findAll();
        return careGiverList.stream().map(CareGiverBuilder::toCareGiverDTO)
                .collect(Collectors.toList());
    }

    public CareGiverDetailsDTO findCareGiverById(UUID id){
        Optional<CareGiver> prosumerOptional = careGiverRepository.findById(id);
        if(!prosumerOptional.isPresent()){
            LOGGER.error("CareGiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(CareGiver.class.getSimpleName()+" with id: "+id);
        }
        return CareGiverBuilder.toCareGiverDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(CareGiverDetailsDTO careGiverDTO){
        CareGiver careGiver = CareGiverBuilder.toEntity(careGiverDTO);
        careGiver = careGiverRepository.save(careGiver);
        LOGGER.debug("CareGiver with id {} was inserted in db",careGiver.getId());
        return careGiver.getId();
    }

    public UUID delete(String name) {
        List<CareGiver> nurseList = careGiverRepository.findByName(name);
        Optional<CareGiver> nurse = careGiverRepository.findById(nurseList.get(0).getId());
        UUID id = nurse.get().getId();

        careGiverRepository.delete(nurse.get());
        return  id;
    }


    public UUID update1(String name, CareGiverDetailsDTO careGiverDetailsDTO){

        CareGiver careGiver = CareGiverBuilder.toEntity(careGiverDetailsDTO);
        List<CareGiver> careGiverList = careGiverRepository.findByName(name);

       // careGiverList.get(0).setPatientList(careGiverDetailsDTO.getPatientList());

        careGiver = careGiverRepository.save(careGiverList.get(0));

        return careGiver.getId();
    }

    public UUID update(String name, CareGiverDetailsDTO careGiverDetailsDTO) {

        CareGiver careGiver = CareGiverBuilder.toEntity(careGiverDetailsDTO);
        List<CareGiver> careGiverList = careGiverRepository.findByName(name);

        careGiverList.get(0).setAddress(careGiverDetailsDTO.getAddress());
        careGiverList.get(0).setName(careGiverDetailsDTO.getName());
        careGiverList.get(0).setDate(careGiverDetailsDTO.getDate_of_birth());
        careGiverList.get(0).setGender(careGiverDetailsDTO.getGender());

        careGiver = careGiverRepository.save(careGiverList.get(0));

        return careGiver.getId();

    }

    public CareGiverDetailsDTO findByName (String name){
        List<CareGiver> nurseList = careGiverRepository.findByName(name);
        Optional<CareGiver> nurse = careGiverRepository.findById(nurseList.get(0).getId());

        CareGiverDetailsDTO careGiverDetailsDTO = new CareGiverDetailsDTO(nurse.get().getId(),nurse.get().getName(),nurse.get().getAddress(),nurse.get().getDate(),nurse.get().getGender());

        return careGiverDetailsDTO;
    }

}
