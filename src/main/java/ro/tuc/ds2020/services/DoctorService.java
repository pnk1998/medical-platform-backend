package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DoctorDto;
import ro.tuc.ds2020.dtos.DoctorDtoDetails;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.repositories.DoctorRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DoctorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorService.class);
    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository){this.doctorRepository = doctorRepository;}

    public List<DoctorDto> findDoctors(){
        List<Doctor> doctorList = doctorRepository.findAll();
        return doctorList.stream()
                .map(DoctorBuilder::toDoctorDTO)
                .collect(Collectors.toList());
    }

    public DoctorDtoDetails findDoctorById(UUID id){
        Optional<Doctor> prosumerOptional = doctorRepository.findById(id);
        if(!prosumerOptional.isPresent()){
            LOGGER.error("Doctor with this id doesn't exists");
            throw new ResourceNotFoundException(Doctor.class.getSimpleName() + " with id: "+ id);
        }

        return DoctorBuilder.toDoctorDtoDetails(prosumerOptional.get());
    }

    public UUID insert(DoctorDtoDetails doctorDtoDetails) {
        Doctor doctor = DoctorBuilder.toEntity(doctorDtoDetails);
        doctor = doctorRepository.save(doctor);
        LOGGER.debug("Patient with id {} was inserted in db", doctor.getId());
        return doctor.getId();
    }

    public UUID delete(String name){
        List<Doctor> doctorList = doctorRepository.findByName(name);
        Optional<Doctor> doctor = doctorRepository.findById(doctorList.get(0).getId());
        UUID id = doctor.get().getId();

        doctorRepository.delete(doctor.get());
        return  id;
    }

    public UUID update(String name, DoctorDtoDetails doctorDtoDetails){
        Doctor doctor = DoctorBuilder.toEntity(doctorDtoDetails);
        List<Doctor> doctorList = doctorRepository.findByName(name);

        doctorList.get(0).setName(doctorDtoDetails.getName());
        doctorList.get(0).setAddress(doctorDtoDetails.getAddress());
        doctorList.get(0).setDate_of_birth(doctorDtoDetails.getDate_of_birth());
        doctorList.get(0).setGender(doctorDtoDetails.getGender());

        doctor = doctorRepository.save(doctorList.get(0));

        return doctor.getId();
    }

}
