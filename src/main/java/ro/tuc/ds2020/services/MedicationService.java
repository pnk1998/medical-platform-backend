package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.CareGiver;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository){this.medicationRepository=medicationRepository;}

    public List<Medication> findMedicationForPatient(){
        List<Medication> medicationList = medicationRepository.findAll();
        return medicationList;
    }
    public List<MedicationDTO> findMedications(){
        List<Medication> medicationList = medicationRepository.findAll();
        return medicationList.stream()
                .map(MedicationBuilder::toMedicationDTO)
                .collect(Collectors.toList());
    }

    public MedicationDetailsDTO findMedicationById(UUID id){
        Optional<Medication> prosumerOptional = medicationRepository.findById(id);
        if(!prosumerOptional.isPresent()){
            LOGGER.error("Medication with id {} was not found in db",id);
            throw new ResourceNotFoundException(Medication.class.getSimpleName()+"with id:"+ id);
        }
        return MedicationBuilder.toMedicationDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(MedicationDetailsDTO medicationDetailsDTO){
        Medication medication = MedicationBuilder.toEntity(medicationDetailsDTO);
        medication = medicationRepository.save(medication);
        LOGGER.debug("Medication with id {} was inserted in db", medication.getId());
        return medication.getId();
    }

    public UUID delete(String name){
        List<Medication> medicationList = medicationRepository.findByName(name);
        Optional<Medication> medication = medicationRepository.findById(medicationList.get(0).getId());
        UUID id = medication.get().getId();
        medicationRepository.delete(medication.get());
        return id;
    }

    public UUID update1 (String name, MedicationDetailsDTO medicationDetailsDTO){

        Medication medication = MedicationBuilder.toEntity(medicationDetailsDTO);
        List<Medication> medicationList = medicationRepository.findByName(name);

        medicationList.get(0).setMedPlan(medicationDetailsDTO.getMedPlan());
        medication = medicationRepository.saveAndFlush(medicationList.get(0));

        return medication.getId();
    }

    public UUID update (String name, MedicationDetailsDTO medicationDetailsDTO){

        Medication medication = MedicationBuilder.toEntity(medicationDetailsDTO);
        List<Medication> medicationList = medicationRepository.findByName(name);

        medicationList.get(0).setDosage(medicationDetailsDTO.getDosage());
        medicationList.get(0).setEffects(medicationDetailsDTO.getEffects());
        medicationList.get(0).setName(medicationDetailsDTO.getName());

        medication = medicationRepository.save(medicationList.get(0));

        return medication.getId();
    }

    public MedicationDetailsDTO findByName (String name){
        List<Medication> medicationList = medicationRepository.findByName(name);
        Optional<Medication> medication = medicationRepository.findById(medicationList.get(0).getId());

        MedicationDetailsDTO medicationDetailsDTO = new MedicationDetailsDTO(medication.get().getId(),medication.get().getName(),medication.get().getEffects(),medication.get().getDosage());

        return medicationDetailsDTO;
    }
}
