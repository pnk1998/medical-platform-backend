package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedPlanDTO;
import ro.tuc.ds2020.dtos.MedPlanDetailsDTO;
import ro.tuc.ds2020.dtos.builders.MedPlanBuilder;
import ro.tuc.ds2020.entities.MedPlan;
import ro.tuc.ds2020.repositories.MedPlanRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class HessyanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedPlanService.class);
    private final MedPlanRepository medPlanRepository;

    @Autowired
    public HessyanService(MedPlanRepository medPlanRepository){this.medPlanRepository = medPlanRepository;}

    public List<MedPlanDTO> findMedPlan(){
        List<MedPlan> medPlanList = medPlanRepository.findAll();
        return medPlanList.stream().map(MedPlanBuilder::toMedPlanDTO1)
                .collect(Collectors.toList());
    }

    public MedPlanDetailsDTO findMedPlanById(UUID id){
        Optional<MedPlan> prosumerOptional = medPlanRepository.findById(id);
        if(!prosumerOptional.isPresent()){
            LOGGER.error("Couldn't find the med Plan");
            throw new ResourceNotFoundException(MedPlan.class.getSimpleName()+" with id: "+id);
        }

        return MedPlanBuilder.toMedPlanDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(MedPlanDetailsDTO medPlanDetailsDTO){
        MedPlan medPlan = MedPlanBuilder.toEntity(medPlanDetailsDTO);
        medPlan = medPlanRepository.save(medPlan);

        return medPlan.getId();
    }

    public UUID update(MedPlanDetailsDTO medPlanDetailsDTO){
        MedPlan medPlan = MedPlanBuilder.toEntity(medPlanDetailsDTO);
        medPlan = medPlanRepository.saveAndFlush(medPlan);

        return medPlan.getId();
    }
}
