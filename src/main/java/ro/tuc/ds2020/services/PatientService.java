package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CareGiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.CareGiverBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.CareGiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {this.patientRepository = patientRepository;}

    public List<PatientDTO> findPatients(){
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public List<Patient> findPatientsForCareGiver(){
        List<Patient> patientList = patientRepository.findAll();
        return  patientList;
    }


    public PatientDetailsDTO findPatientById(UUID id) {
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        System.out.println("AICI "+ prosumerOptional.get().toString());

        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPatientDetailsDTO(prosumerOptional.get());
    }

    public UUID insert(PatientDetailsDTO patientDetailsDTO) {
        Patient patient = PatientBuilder.toEntity(patientDetailsDTO);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public UUID delete(String name){
        List<Patient> patientList = patientRepository.findByName(name);
        Optional<Patient> patient = patientRepository.findById(patientList.get(0).getId());
        UUID id = patient.get().getId();
        patientRepository.delete(patient.get());
        return id;
    }

    public UUID update2(String name, PatientDetailsDTO patientDetailsDTO){

        Patient patient = PatientBuilder.toEntity(patientDetailsDTO);
        List<Patient> patientList = patientRepository.findByName(name);

        patientList.get(0).setMedPlan(patientDetailsDTO.getMedPlan());

        patient = patientRepository.save(patientList.get(0));

        return patient.getId();
    }

    public UUID update1(String name, PatientDetailsDTO patientDetailsDTO){

        Patient patient = PatientBuilder.toEntity(patientDetailsDTO);
        List<Patient> patientList = patientRepository.findByName(name);

        patientList.get(0).setCareGiver(patientDetailsDTO.getCareGiver());
        patient = patientRepository.save(patientList.get(0));

        return patient.getId();
    }

    public UUID update (String name,PatientDetailsDTO patientDetailsDTO){

        Patient patient = PatientBuilder.toEntity(patientDetailsDTO);
        List<Patient> patientList = patientRepository.findByName(name);

        patientList.get(0).setAddress(patientDetailsDTO.getAddress());
        patientList.get(0).setName(patientDetailsDTO.getName());
        patientList.get(0).setDate(patientDetailsDTO.getDate());
        patientList.get(0).setGender(patientDetailsDTO.getGender());
        patientList.get(0).setMed_rec(patientDetailsDTO.getMed_rec());

        patient = patientRepository.save(patientList.get(0));

        return patient.getId();
    }

    public PatientDetailsDTO findByName (String name){
        List<Patient> patientList = patientRepository.findByName(name);
        Optional<Patient> patient = patientRepository.findById(patientList.get(0).getId());

        PatientDetailsDTO patientDetailsDTO = new PatientDetailsDTO(patient.get().getId(),patient.get().getName(),patient.get().getAddress(),patient.get().getDate(),patient.get().getGender(),patient.get().getMed_rec());

        return patientDetailsDTO;
    }
}
