package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.CareGiver;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CareGiverRepository extends JpaRepository<CareGiver, UUID> {

    List<CareGiver> findByName(String name);


    @Query(value = "SELECT p " +
            "FROM CareGiver p " +
            "WHERE p.name = :name " +
            "AND p.date_of_birth = :date_of_birth ")
    Optional<CareGiver> findSeniorsByName(@Param("name") String name);

}
