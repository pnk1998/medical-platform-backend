package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.MedPlan;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MedPlanRepository extends JpaRepository<MedPlan, UUID> {

//    List<MedPlan> findByName(String intervals);
//    @Query(value = "SELECT p " +
//            "FROM MedPlan p " +
//            "WHERE p.intervals_for_pills = :intervals_for_pills ")
//    Optional<MedPlan> findSeniorsByName(@Param("intervals_for_pills") String intervals);

}