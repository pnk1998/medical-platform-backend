package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value="/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService){this.patientService=patientService;}

    @GetMapping()
    public ResponseEntity<List<PatientDTO>> getPatients() {
        List<PatientDTO> dtos = patientService.findPatients();
        for (PatientDTO dto : dtos) {
            Link patientLink = linkTo(methodOn(PatientController.class)
                    .getPatient(dto.getId())).withRel("patientDetails");
            dto.add(patientLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody PatientDetailsDTO patientDetailsDTO) {
        UUID patientID = patientService.insert(patientDetailsDTO);
        return new ResponseEntity<>(patientID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PatientDetailsDTO> getPatient(@PathVariable("id") UUID patientId) {
        PatientDetailsDTO dto = patientService.findPatientById(patientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value ="/{name}")
    public ResponseEntity<UUID> deletePatient(@PathVariable("name") String name){
        UUID id =patientService.delete(name);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }

    @PutMapping(value="/{name_for_update}")
    public ResponseEntity<UUID> updatePatient(@PathVariable("name_for_update") String name_for_update, @Valid @RequestBody PatientDetailsDTO patientDetailsDTO ){
        UUID patientID = patientService.update(name_for_update,patientDetailsDTO);
        return new ResponseEntity<>(patientID,HttpStatus.OK);
    }
}
