package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.MedPlan;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.services.MedPlanService;
import ro.tuc.ds2020.services.MedicationService;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/medplan")
public class MedPlanController {

    private final MedPlanService medPlanService;
    private final PatientService patientService;
    private final MedicationService medicationService;

    @Autowired
    public MedPlanController(MedPlanService medPlanService, PatientService patientService, MedicationService medicationService) {
        this.medPlanService = medPlanService;
        this.patientService = patientService;
        this.medicationService = medicationService;
    }

    @GetMapping
    public ResponseEntity<List<MedPlanDTO>> getMedPlans() {
        List<MedPlanDTO> dtos = medPlanService.findMedPlan();
        for (MedPlanDTO dto : dtos) {
            Link medPlanLink = linkTo(methodOn(MedPlanController.class)
                    .getMedPlan(dto.getId())).withRel("medPlanDetails");
            dto.add(medPlanLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody MedPlanDetailsDTO medPlanDetailsDTO) {
        UUID medPlanID = medPlanService.insert(medPlanDetailsDTO);
        return new ResponseEntity<>(medPlanID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedPlanDetailsDTO> getMedPlan(@PathVariable("id") UUID medPlanID) {
        MedPlanDetailsDTO dto = medPlanService.findMedPlanById(medPlanID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping(value = "/createMedPlan")
    public String insertMedicalPlan(@RequestParam(value = "numeMedicament") String numeMedicament, @Valid @RequestBody MedPlanDetailsDTO medPlanDetailsDTO) {

        MedicationDetailsDTO medicationDetailsDTO = medicationService.findByName(numeMedicament);
        medicationDetailsDTO = medicationService.findMedicationById(medicationDetailsDTO.getId());

        Medication medication = new Medication(medicationDetailsDTO.getId(), medicationDetailsDTO.getName(), medicationDetailsDTO.getEffects(), medicationDetailsDTO.getDosage());
        MedPlan medPlan = new MedPlan(medPlanDetailsDTO.getId(), medPlanDetailsDTO.getStart_hour(), medPlanDetailsDTO.getEnd_hour(),medPlanDetailsDTO.getStart_period(),medPlanDetailsDTO.getEnd_period());

        medicationDetailsDTO.setMedPlan(medPlan);

        if (medPlanDetailsDTO.getMedicationList() != null) {
            medPlanDetailsDTO.getMedicationList().add(medication);
        } else {
            medPlanDetailsDTO.setMedicationList(medPlan.getMedicationList());
        }
        medicationDetailsDTO.getMedPlan().setId(medPlanService.update(medPlanDetailsDTO));
        medicationService.update1(medicationDetailsDTO.getName(), medicationDetailsDTO);

        return " Medplan creat";

    }

    @PostMapping(value = "/createMedicationPlan")
    public String insertMedicalPlanPatient(@RequestParam(value = "numeMedicament") String numeMedicament, @RequestParam(value = "numePacient") String numePacient, @Valid @RequestBody MedPlanDetailsDTO medPlanDetailsDTO) {

        PatientDetailsDTO patientDetailsDTO = patientService.findByName(numePacient);
        patientDetailsDTO = patientService.findPatientById(patientDetailsDTO.getId());

        MedicationDetailsDTO medicationDetailsDTO = medicationService.findByName(numeMedicament);
        medicationDetailsDTO = medicationService.findMedicationById(medicationDetailsDTO.getId());

        Medication medication = new Medication(medicationDetailsDTO.getId(), medicationDetailsDTO.getName(), medicationDetailsDTO.getEffects(), medicationDetailsDTO.getDosage());
        MedPlan medPlan = new MedPlan(medPlanDetailsDTO.getId(), medPlanDetailsDTO.getStart_hour(), medPlanDetailsDTO.getEnd_hour(),medPlanDetailsDTO.getStart_period(),medPlanDetailsDTO.getEnd_period());

        medicationDetailsDTO.setMedPlan(medPlan);

        if (medPlanDetailsDTO.getMedicationList() != null) {
            medPlanDetailsDTO.getMedicationList().add(medication);
        } else {
            medPlanDetailsDTO.setMedicationList(medPlan.getMedicationList());
        }

        medicationDetailsDTO.getMedPlan().setId(medPlanService.update(medPlanDetailsDTO));
        medicationService.update1(medicationDetailsDTO.getName(), medicationDetailsDTO);

        patientDetailsDTO.setMedPlan(medPlan);
        patientService.update2(patientDetailsDTO.getName(),patientDetailsDTO);
        medicationService.update1(medicationDetailsDTO.getName(), medicationDetailsDTO);
        return " Medplan creat";

    }


}
