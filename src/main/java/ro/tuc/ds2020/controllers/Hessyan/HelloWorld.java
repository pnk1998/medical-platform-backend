package ro.tuc.ds2020.controllers.Hessyan;

import ro.tuc.ds2020.dtos.MedPlanDTO;

import java.util.List;

public interface HelloWorld {
    public String sayHelloWithHessian(String msg);
    public List<MedPlanDTO> getMedPlansToday();
}
