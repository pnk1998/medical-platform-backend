package ro.tuc.ds2020.controllers.Hessyan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import ro.tuc.ds2020.controllers.MedPlanController;
import ro.tuc.ds2020.services.HessyanService;

@Configuration
public class HessianConfiguration {

    private final HessyanService hessyanService;

    public HessianConfiguration(HessyanService hessyanService) {
       this.hessyanService = hessyanService;
    }

    @Bean(name = "/hellohessian")
    RemoteExporter sayHelloServiceHessian() {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(new HelloWorldImpl(hessyanService));
        exporter.setServiceInterface(HelloWorld.class);
        return exporter;
    }

}
