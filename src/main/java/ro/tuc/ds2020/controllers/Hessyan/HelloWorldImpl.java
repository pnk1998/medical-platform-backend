package ro.tuc.ds2020.controllers.Hessyan;

import org.springframework.hateoas.Link;
import ro.tuc.ds2020.controllers.MedPlanController;
import ro.tuc.ds2020.controllers.MedicationController;
import ro.tuc.ds2020.dtos.MedPlanDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.entities.MedPlan;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.services.HessyanService;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class HelloWorldImpl implements HelloWorld {

    private final HessyanService hessyanService;

    public HelloWorldImpl(HessyanService hessyanService) {
        this.hessyanService = hessyanService;
    }

    public List<MedPlanDTO> getMedPlansToday(){
        List<MedPlanDTO> dtos = hessyanService.findMedPlan();
 //       dtos.get(0).setEnd_hour(new ArrayList<Time>());
  //      dtos.get(0).setStart_hour(new ArrayList<Time>());
  //      dtos.get(0).setMedicationList(new ArrayList<Medication>());
//        for (MedPlanDTO dto : dtos) {
//            Link medPlanLink = linkTo(methodOn(MedPlanController.class)
//                    .getMedPlan(dto.getId())).withRel("medPlanDetails");
//            dto.add(medPlanLink);
//        }
        return dtos;
    }

    public String sayHelloWithHessian(String msg) {
        System.out.println("=============server side==============");
        System.out.println("msg : " + msg);
        return "Hello " + msg + " Response time :: " + new Date();
    }

}
