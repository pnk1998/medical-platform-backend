package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationDetailsDTO;

import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.services.MedicationService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value= "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService){this.medicationService = medicationService;}

    @GetMapping()
    public ResponseEntity<List<MedicationDTO>> getMedications() {
        List<MedicationDTO> dtos = medicationService.findMedications();
        for (MedicationDTO dto : dtos) {
            Link medicationLink = linkTo(methodOn(MedicationController.class)
                    .getMedication(dto.getId())).withRel("medicationDetails");
            dto.add(medicationLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody MedicationDetailsDTO medicationDetailsDTODTO) {
        UUID personID = medicationService.insert(medicationDetailsDTODTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationDetailsDTO> getMedication(@PathVariable("id") UUID medicationId) {
        MedicationDetailsDTO dto = medicationService.findMedicationById(medicationId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value ="/{name}")
    public ResponseEntity<UUID> deleteMedication(@PathVariable("name") String name){
        UUID id = medicationService.delete(name);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }

    @PutMapping(value="/{name}")
    public ResponseEntity<UUID> updateMedication(@PathVariable("name") String name, @Valid @RequestBody MedicationDetailsDTO medicationDetailsDTO ){
        UUID medicationID = medicationService.update(name,medicationDetailsDTO);
        return new ResponseEntity<>(medicationID,HttpStatus.OK);
    }
}