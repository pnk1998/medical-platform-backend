package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.CareGiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.services.CareGiverService;
import ro.tuc.ds2020.services.PatientService;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CareGiverController {

    private final CareGiverService careGiverService;
    private final PatientService patientService;

    @Autowired
    public CareGiverController(CareGiverService careGiverService,PatientService patientService) {
        this.careGiverService = careGiverService;
        this.patientService = patientService;
    }

    @GetMapping
    public ResponseEntity<List<CareGiverDTO>> getCareGivers() {
        List<CareGiverDTO> dtos = careGiverService.findCareGiver();
        for (CareGiverDTO dto : dtos) {
            Link careGiverLink = linkTo(methodOn(CareGiverController.class)
                    .getCareGiver(dto.getId())).withRel("careGiverDetails");
            dto.add(careGiverLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody CareGiverDetailsDTO careGiverDTO){
        UUID caregiverID = careGiverService.insert(careGiverDTO);
        return new ResponseEntity<>(caregiverID, HttpStatus.CREATED);
    }

    @GetMapping(value= "/{id}")
    public ResponseEntity<CareGiverDetailsDTO> getCareGiver(@PathVariable("id") UUID careGiverID){
        CareGiverDetailsDTO dto = careGiverService.findCareGiverById(careGiverID);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @DeleteMapping(value ="/{name}")
    public ResponseEntity<UUID> deleteCareGiver(@PathVariable("name") String name){
        UUID id =careGiverService.delete(name);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }


    @PutMapping(value="/{name}")
    public ResponseEntity<UUID> updateCareGiver(@PathVariable("name") String name, @Valid @RequestBody CareGiverDetailsDTO careGiverDetailsDTO ){
        UUID careGiverId = careGiverService.update(name,careGiverDetailsDTO);
        return new ResponseEntity<>(careGiverId,HttpStatus.OK);
    }

    @Transactional
    @PostMapping(value="/insertPatient")
    public ResponseEntity<PatientDetailsDTO> insertPatientToCareGiver(@RequestParam (value="numePacient") String numePaccient, @RequestParam(value="numeCareGiver")String numeCareGiver){

        CareGiverDetailsDTO careGiverDTO= careGiverService.findByName(numeCareGiver);
        PatientDetailsDTO patientDTO= patientService.findByName(numePaccient);
        careGiverDTO = careGiverService.findCareGiverById(careGiverDTO.getId());
        patientDTO = patientService.findPatientById(patientDTO.getId());

        CareGiver careGiver = new CareGiver(careGiverDTO.getId(),careGiverDTO.getName(),careGiverDTO.getAddress(),careGiverDTO.getDate_of_birth(),careGiverDTO.getGender());

        patientDTO.setCareGiver(careGiver);
        patientService.update1(numePaccient,patientDTO);

//        if(careGiverDTO.getPatientList()!=null) {
//            careGiverDTO.getPatientList().add(patient);
//            careGiverService.update1(numeCareGiver, careGiverDTO);
//        }else{
//            careGiverDTO.setPatientList(careGiver.getPatientList());
//            careGiverService.update1(numeCareGiver, careGiverDTO);
//        }
        System.out.println("reusit");
        System.out.println(careGiverDTO.getName()+ " este asistenta / asistentul lui "+ patientDTO.getName());

        return new ResponseEntity<>(patientDTO,HttpStatus.OK);
    }

    @Transactional
    @GetMapping(value= "/findPacients/{name}")
    public ResponseEntity<List<PatientDTO>> findPatientsForCareGiver(@PathVariable("name") String name){

        CareGiverDetailsDTO careGiverDetailsDTO = careGiverService.findByName(name);
        careGiverDetailsDTO = careGiverService.findCareGiverById(careGiverDetailsDTO.getId());

        String show = "";
        List<PatientDTO> patientList = patientService.findPatients();
        List<PatientDTO> patientListForCareGiver = new ArrayList<PatientDTO>();

        for(PatientDTO p : patientList){
            if(p.getCareGiver().getId().equals(careGiverDetailsDTO.getId())){
                patientListForCareGiver.add(p);
                show = show + " Pacientul " + p.getName() + " apartine de asistentul " + careGiverDetailsDTO.getName() + "\n";
            }
        }

        System.out.println( " AICI " + show + " size " +patientListForCareGiver.size());
        return new ResponseEntity<>(patientListForCareGiver,HttpStatus.OK);
    }

}
