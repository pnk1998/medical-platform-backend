package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.MedPlan;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.DoctorService;
import ro.tuc.ds2020.services.MedPlanService;
import ro.tuc.ds2020.services.MedicationService;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final DoctorService doctorService;
    private final PatientService patientService;
    private final MedicationService medicationService;
    private final MedPlanService medPlanService;

    @Autowired
    public DoctorController(DoctorService doctorService, PatientService patientService, MedicationService medicationService, MedPlanService medPlanService) {
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.medicationService = medicationService;
        this.medPlanService = medPlanService;
    }

    @GetMapping()
    public ResponseEntity<List<DoctorDto>> getDoctors() {
        List<DoctorDto> dtos = doctorService.findDoctors();
        for (DoctorDto dto : dtos) {
            Link doctorLink = linkTo(methodOn(DoctorController.class)
                    .getDoctor(dto.getId())).withRel("doctorDetails");
            dto.add(doctorLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody DoctorDtoDetails doctorDtoDetails) {
        UUID doctorID = doctorService.insert(doctorDtoDetails);
        return new ResponseEntity<>(doctorID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DoctorDtoDetails> getDoctor(@PathVariable("id") UUID doctortId) {
        DoctorDtoDetails dto = doctorService.findDoctorById(doctortId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{name}")
    public ResponseEntity<UUID> deleteDoctor(@PathVariable("name") String name) {
        UUID id = doctorService.delete(name);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }

    @PutMapping(value = "/{name}")
    public ResponseEntity<UUID> updateDoctor(@PathVariable("name") String name, @Valid @RequestBody DoctorDtoDetails doctorDtoDetails) {
        UUID doctorID = doctorService.update(name, doctorDtoDetails);
        return new ResponseEntity<>(doctorID, HttpStatus.OK);
    }

    @GetMapping(value = "/showInfo/{name}")
    public String showInfo(@PathVariable("name") String name) {

        List<Patient> patientList = patientService.findPatientsForCareGiver();
        Patient patient = new Patient();

        for (Patient p : patientList) {
            if (p.getName().contentEquals(name)) {
                patient = p;
            }
        }

        System.out.println("HAI CU PACIENTUL " + patient.toString());

        List<Medication> medicationList = medicationService.findMedicationForPatient();


        for (Medication m : medicationList) {
            // if(m.getId().equals(medPlanDetailsDTO.getMedicationList().get(0).getId())){
            System.out.println("HAIDAAA " + patient.getMedPlan().getMedicationList());
            System.out.println("IAR AICI " + patient.getMedPlan().getMedicationList().get(0).getId());
            System.out.println("AICI PENTRU VOI " + m.getId());
                        //  }
        }

        return "O mers";
    }
}
