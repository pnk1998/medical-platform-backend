package ro.tuc.ds2020.dtos;

import com.sun.istack.NotNull;
import ro.tuc.ds2020.entities.CareGiver;
import ro.tuc.ds2020.entities.MedPlan;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class PatientDetailsDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String address;
    @NotNull
    private Date date;
    @NotNull
    private String gender;
    @NotNull
    private String med_rec;
    private CareGiver careGiver;
    private MedPlan medPlan;

    public PatientDetailsDTO() {
    }

    public PatientDetailsDTO(String name, String address, Date date, String gender, String med_rec) {
        this.name = name;
        this.address = address;
        this.date = date;
        this.gender = gender;
        this.med_rec = med_rec;
    }

    public PatientDetailsDTO(UUID id, String name, String address, Date date, String gender, String med_rec) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.date = date;
        this.gender = gender;
        this.med_rec = med_rec;
    }

    public PatientDetailsDTO(String name, String address, Date date, String gender, String med_rec, CareGiver careGiver) {
        this.name = name;
        this.address = address;
        this.date = date;
        this.gender = gender;
        this.med_rec = med_rec;
        this.careGiver = careGiver ;
    }

    public PatientDetailsDTO(UUID id, String name, String address, Date date, String gender, String med_rec, CareGiver careGiver) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.date = date;
        this.gender = gender;
        this.med_rec = med_rec;
        this.careGiver = careGiver;
    }

    public PatientDetailsDTO(String name, String address, Date date, String gender, String med_rec, CareGiver careGiver, MedPlan medPlan) {
        this.name = name;
        this.address = address;
        this.date = date;
        this.gender = gender;
        this.med_rec = med_rec;
        this.careGiver = careGiver ;
    }

    public PatientDetailsDTO(UUID id, String name, String address, Date date, String gender, String med_rec, CareGiver careGiver, MedPlan medPlan) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.date = date;
        this.gender = gender;
        this.med_rec = med_rec;
        this.careGiver = careGiver;
    }


    public MedPlan getMedPlan() {
        return medPlan;
    }

    public void setMedPlan(MedPlan medPlan) {
        this.medPlan = medPlan;
    }

    public CareGiver getCareGiver() {
        return careGiver;
    }

    public void setCareGiver(CareGiver careGiver) {
        this.careGiver = careGiver;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMed_rec() {
        return med_rec;
    }

    public void setMed_rec(String med_rec) {
        this.med_rec = med_rec;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDetailsDTO that = (PatientDetailsDTO) o;
        return id.equals(that.id) &&
                name.equals(that.name) &&
                address.equals(that.address) &&
                date.equals(that.date) &&
                gender.equals(that.gender) &&
                med_rec.equals(that.med_rec);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, address, date, gender, med_rec);
    }

    @Override
    public String toString() {
        return "PatientDetailsDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", date=" + date +
                ", gender='" + gender + '\'' +
                ", med_rec='" + med_rec + '\'' +
                ", medPlan=" + medPlan +
                '}';
    }
}
