package ro.tuc.ds2020.dtos;

import com.sun.istack.NotNull;
import ro.tuc.ds2020.entities.MedPlan;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class MedicationDetailsDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String effects;
    @NotNull
    private int dosage;
    private MedPlan medPlan;

    public MedicationDetailsDTO() {

    }

    public MedicationDetailsDTO( String name, String effects, int dosage) {
        this.name = name;
        this.effects = effects;
        this.dosage = dosage;
    }

    public MedicationDetailsDTO(UUID id, String name, String effects, int dosage) {
        this.id = id;
        this.name = name;
        this.effects = effects;
        this.dosage = dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffects() {
        return effects;
    }

    public void setEffects(String effects) {
        this.effects = effects;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public MedPlan getMedPlan() {
        return medPlan;
    }

    public void setMedPlan(MedPlan medPlan) {
        this.medPlan = medPlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDetailsDTO that = (MedicationDetailsDTO) o;
        return dosage == that.dosage &&
                id.equals(that.id) &&
                name.equals(that.name) &&
                effects.equals(that.effects) &&
                medPlan.equals(that.medPlan);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, effects, dosage, medPlan);
    }

    @Override
    public String toString() {
        return "MedicationDetailsDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", effects='" + effects + '\'' +
                ", dosage=" + dosage +
                '}';
    }


}
