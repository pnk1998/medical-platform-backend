package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.DoctorDto;
import ro.tuc.ds2020.dtos.DoctorDtoDetails;
import ro.tuc.ds2020.entities.Doctor;

public class DoctorBuilder {

    private DoctorBuilder(){ }

    public static DoctorDto toDoctorDTO(Doctor doctor){
        return new DoctorDto(doctor.getId(),doctor.getName(),doctor.getAddress(),doctor.getDate_of_birth(),doctor.getGender());
    }

    public static DoctorDtoDetails toDoctorDtoDetails(Doctor doctor){
        return new DoctorDtoDetails(doctor.getId(),doctor.getName(),doctor.getAddress(),doctor.getDate_of_birth(),doctor.getGender());
    }

    public static Doctor toEntity(DoctorDtoDetails doctorDtoDetails){
        return new Doctor(doctorDtoDetails.getName(),
                doctorDtoDetails.getAddress(),
                doctorDtoDetails.getDate_of_birth(),
                doctorDtoDetails.getGender());
    }
}
