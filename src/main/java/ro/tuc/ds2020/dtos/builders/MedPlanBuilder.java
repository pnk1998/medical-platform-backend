package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedPlanDTO;
import ro.tuc.ds2020.dtos.MedPlanDetailsDTO;
import ro.tuc.ds2020.entities.MedPlan;

public class MedPlanBuilder {
    private MedPlanBuilder(){
    }

    public static MedPlanDTO toMedPlanDTO (MedPlan medPlan){
        return new MedPlanDTO(medPlan.getId(),medPlan.getStart_hour(),medPlan.getEnd_hour(),medPlan.getStart_period(),medPlan.getEnd_period());
    }

    public static MedPlanDetailsDTO toMedPlanDetailsDTO (MedPlan medPlan){
        return new MedPlanDetailsDTO(medPlan.getId(),medPlan.getStart_hour(),medPlan.getEnd_hour(),medPlan.getStart_period(),medPlan.getEnd_period());
    }

    public static MedPlanDTO toMedPlanDTO1 (MedPlan medPlan){
        return new MedPlanDTO(medPlan.getId(),medPlan.getStart_hour(),medPlan.getEnd_hour(),medPlan.getStart_period(),medPlan.getEnd_period(),medPlan.getMedicationList());
    }

    public static MedPlan toEntity(MedPlanDetailsDTO medPlanDetailsDTO){
        return new MedPlan(medPlanDetailsDTO.getId(),
                medPlanDetailsDTO.getStart_hour(),
                medPlanDetailsDTO.getEnd_hour(),
                medPlanDetailsDTO.getStart_period(),
                medPlanDetailsDTO.getEnd_period(),
                medPlanDetailsDTO.getMedicationList());
    }
}
