package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationDetailsDTO;
import ro.tuc.ds2020.entities.Medication;

public class MedicationBuilder {

    private MedicationBuilder(){

    }

    public static MedicationDTO toMedicationDTO(Medication medication){
        return new MedicationDTO(medication.getId(),medication.getName(),medication.getEffects(),medication.getDosage());
    }

    public static MedicationDetailsDTO toMedicationDetailsDTO(Medication medication){
        return new MedicationDetailsDTO(medication.getId(),medication.getName(),medication.getEffects(),medication.getDosage());
    }

    public static Medication toEntity(MedicationDetailsDTO medicationDetailsDTO){
        return new Medication(medicationDetailsDTO.getName(),
                            medicationDetailsDTO.getEffects(),
                            medicationDetailsDTO.getDosage());
    }
}
