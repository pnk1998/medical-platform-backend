package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CareGiverDTO;
import ro.tuc.ds2020.dtos.CareGiverDetailsDTO;
import ro.tuc.ds2020.entities.CareGiver;

public class CareGiverBuilder {
    private CareGiverBuilder() {
    }

    public static CareGiverDTO toCareGiverDTO(CareGiver nurse) {
        return new CareGiverDTO(nurse.getId(), nurse.getName(),nurse.getAddress(), nurse.getDate(),nurse.getGender());
    }

    public static CareGiverDetailsDTO toCareGiverDetailsDTO(CareGiver nurse) {
        return new CareGiverDetailsDTO(nurse.getId(), nurse.getName(), nurse.getAddress(), nurse.getDate(),nurse.getGender());
    }

    public static CareGiver toEntity(CareGiverDetailsDTO nurseDetailsDTO) {
        return new CareGiver(nurseDetailsDTO.getName(),
                nurseDetailsDTO.getAddress(),
                nurseDetailsDTO.getDate_of_birth(),
                nurseDetailsDTO.getGender());
    }
}
