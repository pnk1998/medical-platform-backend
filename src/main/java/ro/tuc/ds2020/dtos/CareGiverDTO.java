package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Patient;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class CareGiverDTO extends RepresentationModel<CareGiverDTO> {

    private UUID id;
    private String name;
    private String address;
    private Date date_of_birth;
    private String gender;
//    private List<Patient> patientList;


    public CareGiverDTO(UUID id, String name, String address, Date date, String gender) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.date_of_birth = date;
        this.gender = gender;
    }

    public CareGiverDTO(String name, String address, Date date, String gender) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.date_of_birth = date;
        this.gender = gender;
//        this.patientList = patientList;
    }

    @Override
    public String toString() {
        return "CareGiver " + '\n' +
                "id: " + id + '\n' +
                "name: " + name + '\n' +
                "age: " + date_of_birth + '\n' +
                "gender: " + gender + '\n' +
                "address: " + address + '\n';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CareGiverDTO that = (CareGiverDTO) o;
        return id.equals(that.id) &&
                name.equals(that.name) &&
                address.equals(that.address) &&
                date_of_birth.equals(that.date_of_birth) &&
                gender.equals(that.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, name, address, date_of_birth, gender);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

//    public List<Patient> getPatientList() {
//        return patientList;
//    }
//
//    public void setPatientList(List<Patient> patientList) {
//        this.patientList = patientList;
//    }
}