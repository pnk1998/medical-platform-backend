package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class DoctorDto extends RepresentationModel<DoctorDto> {

    private UUID id;
    private String name;
    private String address;
    private Date date_of_birth;
    private String gender;

    public DoctorDto(UUID id, String name, String address, Date date_of_birth, String gender) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.date_of_birth = date_of_birth;
        this.gender = gender;
    }

    public DoctorDto(String name, String address, Date date_of_birth, String gender) {
        this.name = name;
        this.address = address;
        this.date_of_birth = date_of_birth;
        this.gender = gender;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", date_of_birth=" + date_of_birth +
                ", gender='" + gender + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DoctorDto doctorDto = (DoctorDto) o;
        return id.equals(doctorDto.id) &&
                name.equals(doctorDto.name) &&
                address.equals(doctorDto.address) &&
                date_of_birth.equals(doctorDto.date_of_birth) &&
                gender.equals(doctorDto.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, name, address, date_of_birth, gender);
    }
}
