package ro.tuc.ds2020.dtos;

import com.sun.istack.NotNull;
import org.springframework.hateoas.RepresentationModel;
import ro.tuc.ds2020.entities.Medication;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class MedPlanDTO extends RepresentationModel<MedPlanDTO> implements Serializable {

    private UUID id_medPlan;
    private List<Time> start_hour;
    private List<Time> end_hour;
    private Date start_period;
    private Date end_period;

    private List<Medication> medicationList;

    public MedPlanDTO(){

    }

    public MedPlanDTO(List <Time> start_hour,List <Time> end_hour,Date end_period,Date start_period) {
        this.start_hour = start_hour;
        this.end_hour = end_hour;
        this.start_period = start_period;
        this.end_period = end_period;
    }

    public MedPlanDTO(UUID id, List <Time> start_hour,List <Time> end_hour,Date end_period,Date start_period) {
        this.id_medPlan = id;
        this.start_hour = start_hour;
        this.end_hour = end_hour;
        this.start_period = start_period;
        this.end_period = end_period;
    }

    public MedPlanDTO(UUID id, List <Time> start_hour,List <Time> end_hour,Date end_period,Date start_period, List<Medication> medicationList) {
        this.id_medPlan = id;
        this.start_hour = start_hour;
        this.end_hour = end_hour;
        this.start_period = start_period;
        this.end_period = end_period;
        this.medicationList = medicationList;
    }

    public MedPlanDTO(List <Time> start_hour,List <Time> end_hour,Date end_period,Date start_period, List<Medication> medicationList) {
        this.start_hour = start_hour;
        this.end_hour = end_hour;
        this.start_period = start_period;
        this.end_period = end_period;
        this.medicationList = medicationList;
    }

    public UUID getId() {
        return id_medPlan;
    }

    public void setId(UUID id) {
        this.id_medPlan = id;
    }

    public List<Time> getStart_hour() {
        return start_hour;
    }

    public void setStart_hour(List<Time> start_hour) {
        this.start_hour = start_hour;
    }

    public List<Time> getEnd_hour() {
        return end_hour;
    }

    public void setEnd_hour(List<Time> end_hour) {
        this.end_hour = end_hour;
    }

    public Date getStart_period() {
        return start_period;
    }

    public void setStart_period(Date start_period) {
        this.start_period = start_period;
    }

    public Date getEnd_period() {
        return end_period;
    }

    public void setEnd_period(Date end_period) {
        this.end_period = end_period;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    @Override
    public String toString() {
        return "MedPlanDTO{" +
                "start_hour=" + start_hour +
                ", end_hour=" + end_hour +
                ", start_period=" + start_period +
                ", end_period=" + end_period +
                ", medicationList=" + medicationList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MedPlanDTO that = (MedPlanDTO) o;
        return Objects.equals(start_hour, that.start_hour) &&
                Objects.equals(end_hour, that.end_hour) &&
                Objects.equals(start_period, that.start_period) &&
                Objects.equals(end_period, that.end_period) &&
                Objects.equals(medicationList, that.medicationList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), start_hour, end_hour, start_period, end_period, medicationList);
    }
}
