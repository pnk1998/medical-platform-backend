package ro.tuc.ds2020.dtos;

import com.sun.istack.NotNull;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class DoctorDtoDetails {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String address;
    @NotNull
    private Date date_of_birth;
    @NotNull
    private String gender;

    public DoctorDtoDetails(){

    }

    public DoctorDtoDetails(UUID id, String name, String address, Date date_of_birth, String gender) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.date_of_birth = date_of_birth;
        this.gender = gender;
    }

    public DoctorDtoDetails(String name, String address, Date date_of_birth, String gender) {
        this.name = name;
        this.address = address;
        this.date_of_birth = date_of_birth;
        this.gender = gender;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", date_of_birth=" + date_of_birth +
                ", gender='" + gender + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoctorDtoDetails that = (DoctorDtoDetails) o;
        return id.equals(that.id) &&
                name.equals(that.name) &&
                address.equals(that.address) &&
                date_of_birth.equals(that.date_of_birth) &&
                gender.equals(that.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, address, date_of_birth, gender);
    }
}
